<?php

require_once('../db.php');
$result;
	
	$field = isset($_GET['field']) ? $_GET['field'] : "id_calon";
	$search = isset($_GET['search']) ? $_GET['search'] : "";

	$str_limit = isset($_GET['id_calon']) ? "LIMIT 1" : "";
	$str_limit = isset($_GET['limit']) ? "LIMIT " . $_GET['limit'] : $str_limit;
	$str_id_calon = isset($_GET['id_calon']) ? 
	"AND (pendaftaran.id_calon = '" . $_GET['id_calon'] . "')" : ""; 
	$str_status_kelulusan = isset($_GET['status_kelulusan']) ? 
	("AND nilai.status_kelulusan = " . ($_GET['status_kelulusan'] == "true"
	? "'Diterima'" : "'Tidak Lulus'")) : "";

	$sql = "SELECT nilai.id_nilai, pendaftaran.id_calon, pendaftaran.nama_calon, 
	pendaftaran.pas_foto, pendaftaran.jenis_pendaftaran, nilai.nilai_baca,
	nilai.nilai_tulis, nilai.status_kelulusan  FROM nilai
	INNER JOIN pendaftaran ON pendaftaran.id_calon = nilai.id_calon 
	WHERE nilai.". $field ." LIKE '%". $search ."%' ". $str_id_calon .
	" ". $str_status_kelulusan ." ORDER BY nilai.status_kelulusan ASC, nilai.nilai_baca DESC,
	nilai.nilai_tulis DESC, pendaftaran.id_calon ASC ". $str_limit .";";
	$query = mysqli_query($conn, $sql);
	$data = array();
	$n = 0;
	while($row = mysqli_fetch_array($query)){
		array_push($data,array(
			'id_nilai'			=> $row['id_nilai'],
			'id_calon' 			=> $row['id_calon'],
			'nama_calon' 		=> $row['nama_calon'],
			'jenis_pendaftaran' => $row['jenis_pendaftaran'],
			'nilai_baca' 		=> $row['nilai_baca'],
			'nilai_tulis'		=> $row['nilai_tulis'],
			'pas_foto'		=> $row['pas_foto'],
			'status_kelulusan'	=> $row['status_kelulusan']
		));
		$n++;
	}
	http_response_code(200);
	$result = array('query' => $sql, 'size' => $n, 'data'=> $data, 'message' => "sukses");
	
header('Content-Type: application/json');
echo json_encode($result);	
mysqli_close($conn);

?>
