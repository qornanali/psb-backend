-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 25 Jul 2017 pada 05.07
-- Versi Server: 5.6.14
-- Versi PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `db_sekolah`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akun`
--

CREATE TABLE IF NOT EXISTS `akun` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akun`
--

INSERT INTO `akun` (`username`, `password`, `nama_lengkap`, `email`, `no_hp`, `level`) VALUES
('admin', 'admin', 'administrator', 'me@admin.com', '022-78907878', 'Administrator'),
('aldinnar', 'aldinnar', 'Aldinar Aurelia', 'aldinnar@gmail.com', '08978809297', 'Ketua Yayasan'),
('ryan', 'ryan123', 'Ryan Herdyana', 'ryan@gmail.com', '085693444070', 'Administrator'),
('yayasan', 'yayasan', 'Ketua Yayasan', 'me@yayasan.com', '02292077330', 'Ketua Yayasan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `daftar_ulang`
--

CREATE TABLE IF NOT EXISTS `daftar_ulang` (
  `id_daftarulang` int(11) NOT NULL AUTO_INCREMENT,
  `id_calon` varchar(50) NOT NULL,
  `jenis_pendaftaran` varchar(10) NOT NULL,
  `bukti` varchar(150) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(50) NOT NULL DEFAULT 'Belum Dikonfirmasi',
  PRIMARY KEY (`id_daftarulang`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `daftar_ulang`
--

INSERT INTO `daftar_ulang` (`id_daftarulang`, `id_calon`, `jenis_pendaftaran`, `bukti`, `tanggal_pembayaran`, `status`) VALUES
(8, 'MI-8137', 'MI', 'dashboard-manager.PNG', '2017-06-07 15:59:24', 'Sudah Dikonfirmasi'),
(9, 'MI-0001', 'MI', '', '2017-07-25 09:18:57', 'Belum Dikonfirmasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal_ujian`
--

CREATE TABLE IF NOT EXISTS `jadwal_ujian` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_ujian` varchar(50) NOT NULL,
  `tanggal_ujian` date NOT NULL,
  `waktu_ujian` varchar(100) NOT NULL,
  `jenis_pendaftaran` varchar(50) NOT NULL,
  `ruangan` varchar(50) NOT NULL,
  `status_jadwal` varchar(50) NOT NULL DEFAULT 'Berlaku',
  PRIMARY KEY (`id_jadwal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `jadwal_ujian`
--

INSERT INTO `jadwal_ujian` (`id_jadwal`, `jenis_ujian`, `tanggal_ujian`, `waktu_ujian`, `jenis_pendaftaran`, `ruangan`, `status_jadwal`) VALUES
(1, 'Ujian Tulis', '2017-06-08', '23:00', 'MI', 'MI-1234', 'Berlaku'),
(2, 'Ujian Baca', '2017-06-08', '21:00', 'MI', 'MI-2', 'Berlaku'),
(3, 'Ujian Baca', '2017-07-24', '21:55', 'MI', 'MI-01LOOO', 'Tidak Berlaku');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kuota`
--

CREATE TABLE IF NOT EXISTS `kuota` (
  `id_kuota` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pendaftaran` varchar(50) NOT NULL,
  `tahun` int(50) NOT NULL,
  `jumlah` int(50) NOT NULL,
  PRIMARY KEY (`id_kuota`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `kuota`
--

INSERT INTO `kuota` (`id_kuota`, `jenis_pendaftaran`, `tahun`, `jumlah`) VALUES
(1, 'MI', 2017, 200),
(2, 'MTS', 2017, 150);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_calon` varchar(20) NOT NULL,
  `nama_calon` varchar(50) NOT NULL,
  `jenis_pendaftaran` varchar(5) NOT NULL,
  `nilai_baca` int(10) NOT NULL,
  `nilai_tulis` int(10) NOT NULL,
  `status_kelulusan` varchar(50) NOT NULL DEFAULT 'Tidak Lulus',
  PRIMARY KEY (`id_nilai`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `nilai`
--

INSERT INTO `nilai` (`id_nilai`, `id_calon`, `nama_calon`, `jenis_pendaftaran`, `nilai_baca`, `nilai_tulis`, `status_kelulusan`) VALUES
(1, 'MI-8137', 'aldinnar', 'MI', 100, 100, 'Diterima'),
(3, 'MA-3045', 'aurelia', 'MA', 90, 90, 'Diterima');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftaran`
--

CREATE TABLE IF NOT EXISTS `pendaftaran` (
  `id_calon` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `jenis_pendaftaran` varchar(5) NOT NULL,
  `nama_calon` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `ttl` varchar(100) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `sekolah_asal` varchar(100) NOT NULL,
  `tahun_ijazah` varchar(10) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `anak` varchar(3) NOT NULL,
  `jumlah_saudara` varchar(3) NOT NULL,
  `nama_ayah` varchar(50) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `pekerjaan_ayah` varchar(100) NOT NULL,
  `pekerjaan_ibu` varchar(100) NOT NULL,
  `pendidikan_ayah` varchar(10) NOT NULL,
  `pendidikan_ibu` varchar(10) NOT NULL,
  `alamat_ortu` varchar(150) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `pas_foto` text NOT NULL,
  `foto_ijazah` text NOT NULL,
  `foto_skhun` varchar(500) NOT NULL,
  `akte` text NOT NULL,
  `kartu_keluarga` text NOT NULL,
  `status_pendaftaran` varchar(50) NOT NULL DEFAULT 'Belum Dikonfirmasi',
  PRIMARY KEY (`id_calon`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pendaftaran`
--

INSERT INTO `pendaftaran` (`id_calon`, `email`, `password`, `jenis_pendaftaran`, `nama_calon`, `jenis_kelamin`, `ttl`, `agama`, `sekolah_asal`, `tahun_ijazah`, `alamat`, `anak`, `jumlah_saudara`, `nama_ayah`, `nama_ibu`, `pekerjaan_ayah`, `pekerjaan_ibu`, `pendidikan_ayah`, `pendidikan_ibu`, `alamat_ortu`, `no_hp`, `pas_foto`, `foto_ijazah`, `foto_skhun`, `akte`, `kartu_keluarga`, `status_pendaftaran`) VALUES
('MA-3045', 'aurelia@gmail.com', 'aurelia', 'MA', 'aurelia', 'Perempuan', 'bandung, 31 agustus 2000', 'Islam', 'SMP 3 Bandung', '2017', 'bandung', '2', '2', 'rudi', 'annisa', 'pns', 'pns', 'Sarjana', 'Sarjana', 'bandung', '08978809297', 'deletedoc.PNG', 'deleteuser.PNG', 'docoverview.PNG', 'tidak ada foto', 'tidak ada foto', 'Sudah Dikonfirmasi'),
('MI-0001', 'aliqornan@ymail.com', 'ali', 'MI', 'Ali Qornan Jaisyurrahman', 'Laki-laki', 'bandung, 29 nopember 1997', 'Islam', 'MIN 2 Margasari', '2017', 'komplek griya bandung indah', '3', '5', 'agus', 'imas', 'wiraswasta', 'guru', 'D4', 'S1', 'komplek gbi', '02292077330', 'images/pas_foto/pas_foto_MI-0001.jpg', '', '', '', '', 'Sudah Dikonfirmasi'),
('MI-8137', 'aldinnar@gmail.com', 'aldinnar', 'MI', 'aldinnar', 'Perempuan', 'bandung, 31 agustus 2000', 'Islam', '-', '-', 'Bandung', '2', '3', 'hermawan', 'maryati', 'swasta', 'ibu rumah tangga', 'Sarjana', 'SMA', 'bandung', '081322303863', 'deletedoc.PNG', 'deleteuser.PNG', 'dashboard-admin.PNG', 'deleteprod.PNG', 'dashboard-employee.PNG', 'Sudah Dikonfirmasi'),
('MTS-6549', 'ivanka@gmail.com', 'ivanka', 'MTS', 'ivanka', 'Perempuan', 'bandung, 31 agustus 2000', 'Islam', 'SD bandung', '2017', 'bandung', '2', '3', 'redi', 'nisa', 'pns', 'pns', 'Sarjana', 'Sarjana', 'bandung', '085693444070', 'adduser.PNG', 'dashboard-admin.PNG', 'deleteuser.PNG', 'tidak ada foto', 'tidak ada foto', 'Belum Dikonfirmasi');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
