<?php

require_once('../db.php');
$result;

	$data = array();
	$jumlah = array();

	$sql = "SELECT COUNT(id_calon) AS jumlah FROM pendaftaran;";	
	$query = mysqli_query($conn, $sql);
	$row = mysqli_fetch_array($query);
	$jumlah[0] = $row['jumlah'];
	array_push($data,array(
			'tren_title'		=> 'Jumlah siswa terdaftar',
			'tren_content' 		=> $row['jumlah']
		));

	$sql = "SELECT COUNT(pendaftaran.id_calon) AS jumlah FROM pendaftaran 
	INNER JOIN nilai ON
	nilai.id_calon = pendaftaran.id_calon
	WHERE nilai.status_kelulusan = 'Diterima';";	
	$query = mysqli_query($conn, $sql);
	$row = mysqli_fetch_array($query);
	$jumlah[1] = $row['jumlah'];
	array_push($data,array(
			'tren_title'		=> 'Jumlah siswa diterima',
			'tren_content' 		=> $row['jumlah']
		));
	array_push($data,array(
			'tren_title'		=> 'Siswa tidak diterima',
			'tren_content' 		=> ($jumlah[0] - $jumlah[1]).""	
		));

	$sql = "SELECT COUNT(id_calon) AS jumlah FROM pendaftaran
	WHERE jenis_pendaftaran = 'MI' ;";	
	$query = mysqli_query($conn, $sql);
	$row = mysqli_fetch_array($query);
	$jumlah[1] = $row['jumlah'];
	array_push($data,array(
			'tren_title'		=> 'Jumlah pendaftar MI',
			'tren_content' 		=> $row['jumlah']
		));
	$sql = "SELECT COUNT(id_calon) AS jumlah FROM pendaftaran
	WHERE jenis_pendaftaran = 'MTS' ;";	
	$query = mysqli_query($conn, $sql);
	$row = mysqli_fetch_array($query);
	$jumlah[1] = $row['jumlah'];
	array_push($data,array(
			'tren_title'		=> 'Jumlah pendaftar MTS',
			'tren_content' 		=> $row['jumlah']
		));
	$sql = "SELECT COUNT(id_calon) AS jumlah FROM pendaftaran
	WHERE jenis_pendaftaran = 'MA' ;";	
	$query = mysqli_query($conn, $sql);
	$row = mysqli_fetch_array($query);
	$jumlah[1] = $row['jumlah'];
	array_push($data,array(
			'tren_title'		=> 'Jumlah pendaftar MA',
			'tren_content' 		=> $row['jumlah']
		));
	$sql = "SELECT COUNT(id_calon) AS jumlah FROM daftar_ulang;";	
	$query = mysqli_query($conn, $sql);
	$row = mysqli_fetch_array($query);
	$jumlah[1] = $row['jumlah'];
	array_push($data,array(
			'tren_title'		=> 'Jumlah siswa daftar ulang',
			'tren_content' 		=> $row['jumlah']
		));
	

	http_response_code(200);
	$result = array('query' => $sql, 'size' => 7, 'data'=> $data, 'message' => "sukses");
	
header('Content-Type: application/json');
echo json_encode($result);	
mysqli_close($conn);

?>
