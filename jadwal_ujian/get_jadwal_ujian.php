<?php

require_once('../db.php');
$result;
	
	$field = isset($_GET['field']) ? $_GET['field'] : "id_jadwal";
	$search = isset($_GET['search']) ? $_GET['search'] : "";

	$str_limit = isset($_GET['limit']) ? "LIMIT " . $_GET['limit'] : "";
	$str_jenis_pendaftaran = isset($_GET['jenis_pendaftaran']) ? 
	"AND (jenis_pendaftaran = '" . $_GET['jenis_pendaftaran'] . "')" : ""; 
	$str_status_jadwal = isset($_GET['status_jadwal']) ? 
	("AND status_jadwal = " . ($_GET['status_jadwal'] == "true"
	? "'Berlaku'" : "'Tidak Berlaku'")) : "";

	$sql = "SELECT * FROM jadwal_ujian 
	WHERE ". $field ." LIKE '%". $search ."%' ". $str_jenis_pendaftaran .
	" ". $str_status_jadwal ." ". $str_limit .";";
	$query = mysqli_query($conn, $sql);
	$data = array();
	$n = 0;
	while($row = mysqli_fetch_array($query)){
		array_push($data,array(
			'id_jadwal'			=> $row['id_jadwal'],
			'jenis_ujian' 		=> $row['jenis_ujian'],
			'tanggal_ujian' 	=> $row['tanggal_ujian'],
			'waktu_ujian' 		=> $row['waktu_ujian'],
			'jenis_pendaftaran' => $row['jenis_pendaftaran'],
			'ruangan'			=> $row['ruangan'],
			'status_jadwal'		=> $row['status_jadwal']
		));
		$n++;
	}
	http_response_code(200);
	$result = array('query' => $sql, 'size' => $n, 'data'=> $data, 'message' => "sukses");
	
header('Content-Type: application/json');
echo json_encode($result);	
mysqli_close($conn);

?>
