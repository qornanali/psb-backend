<?php

require_once('../db.php');
$result;
	
	$field = isset($_GET['field']) ? $_GET['field'] : "nama_calon";
	$search = isset($_GET['search']) ? $_GET['search'] : "";

	$str_limit = isset($_GET['username']) || isset($_GET['id_calon']) ? "LIMIT 1" : "";
	$str_limit = isset($_GET['limit']) ? "LIMIT " . $_GET['limit'] : $str_limit;
	$str_username = isset($_GET['username']) ? "AND (email = '". $_GET['username'] ."')" : "";
	$str_id_calon = isset($_GET['id_calon']) ? "AND (id_calon = '". $_GET['id_calon'] ."')" : "";
	$str_verified = isset($_GET['verified']) ? 
	("AND status_pendaftaran = " . ($_GET['verified'] == "true"
	? "'Sudah Dikonfirmasi'" : "'Belum Dikonfirmasi'")) : "";

	$sql = "SELECT id_calon, nama_calon, sekolah_asal, pas_foto, status_pendaftaran, email 
	FROM pendaftaran WHERE ". $field ." LIKE '%". $search ."%' ". $str_username .
	" ". $str_id_calon . " " . $str_verified ." ". $str_limit .";";
	$query =	 mysqli_query($conn, $sql);
	$data = array();
	$n = 0;
	while($row = mysqli_fetch_array($query)){
		array_push($data,array(
			'id_calon'		=> $row['id_calon'],
			'nama_calon' 	=> $row['nama_calon'],
			'sekolah_asal'	=> $row['sekolah_asal'],
			'pas_foto'		=> $row['pas_foto'],
			'email'			=> $row['email'],
			'status_pendaftaran'	=> $row['status_pendaftaran']
		));
		$n++;
	}
	http_response_code(200);
	$result = array('query' => $sql, 'size' => $n, 'data'=> $data, 'message' => "sukses");
	
header('Content-Type: application/json');
echo json_encode($result);	
mysqli_close($conn);

?>
